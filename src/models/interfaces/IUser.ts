import { Document } from "mongoose";
import { Request, Response } from "express";

export interface IUser extends Document {
  name: string;
  email: string;
  country: string;
  creationDate: Date;
}

export interface IUserRequest extends Request {
  name: string;
  email: string;
  country: string;
  creationDate: Date;
}

export interface IUserResponse extends Response {}
