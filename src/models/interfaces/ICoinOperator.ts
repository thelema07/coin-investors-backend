import { Document } from "mongoose";
import { Request, Response } from "express";

// Custom Operator Interfaces

export interface ICoinOperator extends Document {
  name: string;
  lastName: string;
  country: string;
  permission: string;
  clearance: string[];
  creationDate: Date;
}

export interface ICoinOperatorRequest extends Request {
  name: string;
  lastName: string;
  country: string;
  permission: string;
  clearance: string[];
  creationDate: Date;
}

export interface ICoinOperatorResponse extends Response {}
