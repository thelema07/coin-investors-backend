import * as mongoose from "mongoose";
import { ICoinOperator } from "./interfaces/ICoinOperator";

const Schema = mongoose.Schema;

const CoinOperatorSchema = new Schema({
  name: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  country: { type: String, required: true },
  permission: { type: Array, required: true },
  clearance: { type: String, required: true },
  creationDate: { type: Date, required: true },
});

export default mongoose.model<ICoinOperator>(
  "CoinOperator",
  CoinOperatorSchema
);
