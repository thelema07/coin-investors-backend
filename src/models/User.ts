import * as mongoose from "mongoose";
import { IUser } from "./interfaces/IUser";

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  country: { type: String, required: true },
  creationDate: { type: Date, required: true },
});

export default mongoose.model<IUser>("User", UserSchema);
