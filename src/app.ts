import * as express from "express";
import { connectDB } from "./config/db";

import { UserRouter } from "./routes/api/user-route";
import { CoinOperatorRouter } from "./routes/api/coin-operator-route";

const app = express();

app.use("/", (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept,X-HTTP-USER-ID"
  );
  res.header("Access-Control-Allow-Credentials");
  next();
});

// TODO
// Implements and test a function to validate emails
// Test all the current functionality done

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

connectDB();

app.use("/api/users", UserRouter);
app.use("/api/coin-operators", CoinOperatorRouter);

app.get("/", (req, res) => {
  res.send("This is typescript Server new attempt");
});

export { app };
