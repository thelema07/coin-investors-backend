import * as express from "express";

import CoinOperator from "../../models/CoinOperator";
import {
  ICoinOperatorRequest,
  ICoinOperatorResponse,
} from "../../models/interfaces/ICoinOperator";

export const addNewCoinOperator = async (
  req: ICoinOperatorRequest,
  res: ICoinOperatorResponse
) => {
  let { name, lastName, country, email, permission, clearance } = req.body;
  const creationDate = Date.now();
  console.log(req.body);

  try {
    let coinOperator = await CoinOperator.findOne({ email });

    if (coinOperator) {
      return res.status(400).json({ msg: "Coin Operator already Exists" });
    }

    const newCoinOperator = new CoinOperator({
      name,
      lastName,
      country,
      email,
      permission,
      clearance,
      creationDate,
    });
    await newCoinOperator.save();
    res.status(200).json(newCoinOperator);
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const getAllCoinOperators = async (
  req: ICoinOperatorRequest,
  res: ICoinOperatorResponse
) => {
  try {
    const coinOperators = await CoinOperator.find();
    res.status(200).json(coinOperators);
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const getCoinOperatorById = async (
  req: ICoinOperatorRequest,
  res: ICoinOperatorResponse
) => {
  const { id } = req.params;
  try {
    const coinOperator = await CoinOperator.findById(id);
    if (!coinOperator) {
      return res.status(404).json({ msg: "Coin Operator Not found" });
    }
    res.status(200).json(coinOperator);
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "Coin Operator not found" });
    }
    res.status(500).send("Server Error");
  }
};

export const updateCoinOperatorById = async (
  req: ICoinOperatorRequest,
  res: ICoinOperatorResponse
) => {
  const { id } = req.params;
  try {
    await CoinOperator.findByIdAndUpdate(id, { $set: req.body }, { new: true });
    res.status(200).json({ msg: "Coin Operator updated" });
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "Coin Operator not found" });
    }
    res.status(500).send("Server Error");
  }
};

export const deleteCoinOperatorById = async (
  req: ICoinOperatorRequest,
  res: ICoinOperatorResponse
) => {
  const { id } = req.params;
  try {
    const coinOperator = await CoinOperator.findByIdAndDelete(id);
    if (!coinOperator) {
      return res.status(404).json({ msg: "Coin Operator Not found" });
    }
    res.status(200).json({ msg: "Coin Operator Deleted" });
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "Coin Operator not found" });
    }
    res.status(500).send("Server Error");
  }
};
