import * as express from "express";

import User from "../../models/User";
import { IUserRequest, IUserResponse } from "../../models/interfaces/IUser";
import { isEmail } from "../../util/helpers/validation";

export const addNewUser = async (req: IUserRequest, res: IUserResponse) => {
  let { name, email, country } = req.body;
  const creationDate = Date.now();

  if (!isEmail(email)) {
    return res.status(400).json({ msg: "Invalid Email" });
  }

  try {
    let user = await User.findOne({ email });

    if (user) {
      return res.status(400).json({ msg: "User already Exists" });
    }

    const newUser = new User({ name, email, country, creationDate });
    await newUser.save();
    res.status(200).json(newUser);
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const getAllUsers = async (req: IUserRequest, res: IUserResponse) => {
  try {
    const users = await User.find();
    res.status(200).json(users);
  } catch (error) {
    console.log(error.message);
    res.status(500).send("Server Error");
  }
};

export const getUserById = async (req: IUserRequest, res: IUserResponse) => {
  const { id } = req.params;
  try {
    const user = await User.findById(id);
    if (!user) {
      return res.status(404).json({ msg: "User Not found" });
    }
    res.status(200).json(user);
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "User not found" });
    }
    res.status(500).send("Server Error");
  }
};

export const updateUserById = async (req: IUserRequest, res: IUserResponse) => {
  const { id } = req.params;
  try {
    await User.findByIdAndUpdate(id, { $set: req.body }, { new: true });
    res.status(200).json({ msg: "User updated" });
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "User not found" });
    }
    res.status(500).send("Server Error");
  }
};

export const deleteUserById = async (req: IUserRequest, res: IUserResponse) => {
  const { id } = req.params;
  try {
    const user = await User.findByIdAndDelete(id);
    if (!user) {
      return res.status(404).json({ msg: "User Not found" });
    }
    res.status(200).json({ msg: "User Deleted" });
  } catch (error) {
    console.log(error);
    if (error.path === "_id") {
      return res.status(404).json({ msg: "User not found" });
    }
    res.status(500).send("Server Error");
  }
};
