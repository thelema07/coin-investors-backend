import * as request from "supertest";
import * as mongoose from "mongoose";
import { DataConfig } from "../../../config/DataConfig";
import { app } from "../../../app";

beforeAll(async () => {
  await mongoose.connect(DataConfig.mongoURI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
});

afterAll(async () => {
  mongoose.connection.close();
});

test("Can get all the users", async (done) => {
  await request(app).get("/api/users/all").expect(200);
  done();
});

test("Can add a new user", async (done) => {
  const randomNum = Math.floor(Math.random() * 10000);
  await request(app)
    .post("/api/users/add")
    .send({
      name: "Heqt The Earth Protector",
      email: `heqt${randomNum}@test.com`,
      country: "Egypt",
      creationDate: "2020-05-09T16:20:01.216Z",
    })
    .expect(200);
  done();
});
