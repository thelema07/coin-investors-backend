import * as express from "express";
import {
  addNewCoinOperator,
  getAllCoinOperators,
  getCoinOperatorById,
  updateCoinOperatorById,
  deleteCoinOperatorById,
} from "../controllers/coin-operator-controller";

const CoinOperatorRouter = express.Router();

CoinOperatorRouter.post("/add", addNewCoinOperator);
CoinOperatorRouter.get("/all", getAllCoinOperators);
CoinOperatorRouter.get("/:id", getCoinOperatorById);
CoinOperatorRouter.put("/:id", updateCoinOperatorById);
CoinOperatorRouter.delete("/:id", deleteCoinOperatorById);

export { CoinOperatorRouter };
