import * as express from "express";
import {
  addNewUser,
  getAllUsers,
  getUserById,
  deleteUserById,
  updateUserById,
} from "../controllers/user-controller";

const UserRouter = express.Router();

UserRouter.post("/add", addNewUser);
UserRouter.get("/all", getAllUsers);
UserRouter.get("/:id", getUserById);
UserRouter.put("/:id", updateUserById);
UserRouter.delete("/:id", deleteUserById);

export { UserRouter };
