export const isEmail = (email: string) => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email.toLowerCase());
};

export const isVisaCardInput = (cardInput: string) => {
  const regex = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
  return regex.test(cardInput);
};

export const isAmericanExpressCardInput = (cardInput: string) => {
  const regex = /^(?:3[47][0-9]{13})$/;
  return regex.test(cardInput);
};
