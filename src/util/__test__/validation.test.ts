import * as request from "supertest";
import { app } from "../../app";
import {
  isEmail,
  isVisaCardInput,
  isAmericanExpressCardInput,
} from "../helpers/validation";

beforeAll(async () => {});

afterAll(async () => {});

test("Input is not a valid email", async (done) => {
  const emailInput = "luxor@luxorcom";
  try {
    expect(isEmail(emailInput)).toBe(false);
    done();
  } catch (error) {
    done(error);
  }
});

test("Input is a valid email", async (done) => {
  const emailInput = "luxor@luxor.com";
  try {
    await expect(isEmail(emailInput)).toBe(true);
    done();
  } catch (error) {
    done(error);
  }
});

test("Input is an invalid Visa Card", async (done) => {
  const cardInput = "3444232423241234";
  try {
    await expect(isVisaCardInput(cardInput)).toBe(false);
    done();
  } catch (error) {
    done(error);
  }
});

test("Input is an invalid Visa Card", async (done) => {
  const cardInput = "4024007103939509";
  try {
    await expect(isVisaCardInput(cardInput)).toBe(true);
    done();
  } catch (error) {
    done(error);
  }
});

test("Input is an invalid American Express Card", async (done) => {
  const cardInput = "747866628981454";
  try {
    await expect(isAmericanExpressCardInput(cardInput)).toBe(false);
    done();
  } catch (error) {
    done(error);
  }
});

test("Input is an valid American Express Card", async (done) => {
  const cardInput = "341921412626975";
  try {
    await expect(isAmericanExpressCardInput(cardInput)).toBe(true);
    done();
  } catch (error) {
    done(error);
  }
});
