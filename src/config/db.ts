import * as mongoose from "mongoose";
import { DataConfig } from "./DataConfig";

export const connectDB = () => {
  // in this method i want to delete only test users from db
  mongoose.connect(DataConfig.mongoURI, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });

  // Search in users docs for users and count test registered users

  console.log("Coin Investor is ON! --- AKA MongoDB");
};
